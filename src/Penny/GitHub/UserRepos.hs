-- | This module handles pulling in a directory of all of the repos that a user contributes to.
module Penny.GitHub.UserRepos (UserRepos (UserRepos)) where

import Control.Monad (filterM)
import Control.Monad.IO.Class
import Control.Monad.Trans.Except
import qualified Data.ByteString as BS hiding
  ( pack,
  )
import Data.List (isPrefixOf)
import Data.String (fromString)
import Data.Text (unpack)
import qualified Data.Vector as V
import qualified GitHub as GH
import Penny.GitHub.Issues
import Penny.GitHub.Repo
import Penny.Types
import System.Fuse

-- | The repos for a given User
data UserRepos = UserRepos
  { userReposToken :: BS.ByteString,
    userReposName :: String
  }

instance Fileable UserRepos where
  getName = return . userReposName

getRepos :: UserRepos -> IO [Repo]
getRepos user = do
  repos <- GH.github (GH.OAuth . userReposToken $ user) GH.userReposR (fromString . userReposName $ user) GH.RepoPublicityAll 100
  case repos of
    Left _ -> return []
    Right rs -> return . V.toList . V.map (Repo . IssueConfig (userReposToken user) (userReposName user) . unpack . GH.untagName . GH.repoName) $ rs

instance Directable UserRepos where
  getSubs user = do
    repos <- getRepos user
    return $ map DirectType repos
  getDirPath _ "" = throwE eNOENT
  getDirPath user "/" = return . Left . DirectType $ user
  getDirPath user path = do
    repos <- liftIO $ getRepos user
    rs <- filterM match repos
    r <- except . headEither eNOENT $ rs
    getDirPath r . subpath $ path
    where
      match :: (MonadIO m) => Repo -> m Bool
      match r = do
        name <- liftIO $ getName r
        return $ ("/" <> name) `isPrefixOf` path
