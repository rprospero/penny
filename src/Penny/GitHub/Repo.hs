{-# LANGUAGE OverloadedStrings #-}

-- | A filesystem folder that contains all of the issues attached to a repo
module Penny.GitHub.Repo (Repo (Repo)) where

import Control.Monad.Trans.Except
import Data.Bifunctor (Bifunctor (first))
import qualified Data.ByteString as BS hiding
  ( pack,
  )
import Data.List (isPrefixOf)
import Data.String (fromString)
import qualified Data.Vector as V
import qualified GitHub as GH
import Penny.GitHub.Issues
import Penny.GitHub.Pr
import Penny.Types
import System.Fuse
import Text.Read (readEither)

-- | The Type for a Repo
newtype Repo = Repo IssueConfig

repoToken :: Repo -> BS.ByteString
repoToken (Repo r) = icToken r

repoName :: Repo -> String
repoName (Repo r) = icRepoName r

repoUser :: Repo -> String
repoUser (Repo r) = icRepoUser r

backToIssue :: Repo -> IssueConfig
backToIssue (Repo r) = r

getIssues :: Repo -> IO [FileType]
getIssues repo = do
  eissues <-
    GH.github
      (GH.OAuth . repoToken $ repo)
      GH.issuesForRepoR
      (fromString $ repoUser repo)
      (fromString $ repoName repo)
      mempty
      100
  case eissues of
    Left _ -> return []
    Right issues ->
      return
        . V.toList
        . V.map (FileType . IssueFile)
        $ issues

getPrs :: Repo -> IO [FileType]
getPrs repo = do
  eprs <-
    GH.github
      (GH.OAuth . repoToken $ repo)
      GH.pullRequestsForR
      (fromString $ repoUser repo)
      (fromString $ repoName repo)
      mempty
      100
  case eprs of
    Left _ -> return []
    Right prs ->
      return
        . V.toList
        . V.map (FileType . PrFile)
        $ prs

instance Fileable Repo where
  getName = return . repoName

instance Directable Repo where
  getFiles repo = do
    is <- getIssues repo
    rs <- getPrs repo
    return $ is <> rs

  getDirPath _ "" = throwE eNOENT
  getDirPath repo "/" = return . Left . DirectType $ repo
  getDirPath repo (_ : path)
    | "issue_" `isPrefixOf` path = do
      index <- except . first (const eNOENT) . readEither . drop 6 $ path
      result <- withExceptT (const eNOENT) . ExceptT . (getIssue . backToIssue $ repo) $ index
      return . Right . FileType $ result
    | "pr_" `isPrefixOf` path = do
      index <- except . first (const eNOENT) . readEither . drop 3 $ path
      result <- withExceptT (const eNOENT) . ExceptT . (getPr . backToIssue $ repo) $ index
      return . Right . FileType $ result
    | otherwise = throwE eNOENT
