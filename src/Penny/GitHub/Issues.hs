-- | This handles turning a single GitHub issue into a file
module Penny.GitHub.Issues (IssueConfig (..), IssueFile (IssueFile), getIssue) where

import Control.Monad.Trans.Except
import qualified Data.ByteString as BS hiding
  ( pack,
  )
import Data.String (fromString)
import Data.Yaml
import Data.Yaml.Pretty
import qualified GitHub as GH
import Penny.Types

instance ToJSON GH.Issue

instance ToJSON GH.IssueLabel

instance ToJSON GH.Milestone

instance ToJSON GH.PullRequestReference

instance ToJSON GH.SimpleUser

-- | A GitHub issue
newtype IssueFile = IssueFile GH.Issue
  deriving (Show)

-- | All the information needed to access a GitHub issue.
data IssueConfig = IssueConfig
  { icToken :: BS.ByteString,
    icRepoUser :: String,
    icRepoName :: String
  }

instance ToJSON IssueFile where
  toJSON (IssueFile i) = toJSON i

instance Fileable IssueFile where
  toFile = return . encodePretty defConfig
  getTime (IssueFile x) = return $ GH.issueUpdatedAt x
  getName (IssueFile x) = return $ "issue_" <> (show . GH.unIssueNumber . GH.issueNumber $ x)

-- | Download an issue from GitHub
getIssue ::
  -- | Which repo to download the issue from
  IssueConfig ->
  -- | The issue number
  Int ->
  IO (Either GH.Error IssueFile)
getIssue config issue = runExceptT $ do
  is <-
    ExceptT $
      GH.github
        (GH.OAuth . icToken $ config)
        GH.issueR
        (fromString $ icRepoUser config)
        (fromString $ icRepoName config)
        (GH.IssueNumber issue)
  return $ IssueFile is
