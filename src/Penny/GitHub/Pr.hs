-- | This handles turning a single GitHub Pull Request into a file
module Penny.GitHub.Pr (PrFile (PrFile), getPr) where

import Control.Monad.Trans.Except
import Data.String (fromString)
import Data.Yaml
import Data.Yaml.Pretty
import qualified GitHub as GH
import Penny.GitHub.Issues
import Penny.Types

-- | A newtype wrapper on GH.SimplePullRequest for making a Fileable
-- instance
newtype PrFile = PrFile GH.SimplePullRequest
  deriving (Show)

instance ToJSON PrFile where
  toJSON (PrFile x) = toJSON x

instance ToJSON GH.OwnerType

instance ToJSON GH.SimplePullRequest

instance ToJSON GH.PullRequestCommit

instance ToJSON GH.PullRequestLinks

instance ToJSON GH.Repo

instance ToJSON GH.SimpleOwner

instance Fileable PrFile where
  toFile = return . encodePretty defConfig
  getTime (PrFile x) = return $ GH.simplePullRequestUpdatedAt x
  getName (PrFile x) = return $ "pr_" <> (show . GH.unIssueNumber . GH.simplePullRequestNumber $ x)

-- | Download a pull request from GitHub
getPr ::
  -- | Which repo to download the pull request from
  IssueConfig ->
  -- | The pull request number
  Int ->
  IO (Either GH.Error PrFile)
getPr config issue = runExceptT $ do
  pr <-
    ExceptT $
      GH.github
        (GH.OAuth . icToken $ config)
        GH.pullRequestR
        (fromString $ icRepoUser config)
        (fromString $ icRepoName config)
        (GH.IssueNumber issue)
  return . PrFile . simplifyPr $ pr

simplifyPr :: GH.PullRequest -> GH.SimplePullRequest
simplifyPr pr =
  GH.SimplePullRequest
    (GH.pullRequestClosedAt pr)
    (GH.pullRequestCreatedAt pr)
    (GH.pullRequestUser pr)
    (GH.pullRequestPatchUrl pr)
    (GH.pullRequestState pr)
    (GH.pullRequestNumber pr)
    (GH.pullRequestHtmlUrl pr)
    (GH.pullRequestUpdatedAt pr)
    (GH.pullRequestBody pr)
    (GH.pullRequestAssignees pr)
    (GH.pullRequestRequestedReviewers pr)
    (GH.pullRequestIssueUrl pr)
    (GH.pullRequestDiffUrl pr)
    (GH.pullRequestUrl pr)
    (GH.pullRequestLinks pr)
    (GH.pullRequestMergedAt pr)
    (GH.pullRequestTitle pr)
    (GH.pullRequestId pr)
