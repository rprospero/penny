{-# LANGUAGE OverloadedStrings #-}

-- | A filesystem folder that contains all of the stories in an Atom or RSS feed
module Penny.Feed (Cached, cache, access, feed) where

import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Trans.Except
import Data.ByteString.Lazy (fromStrict)
import Data.IORef
import Data.Maybe (fromJust, fromMaybe)
import Data.String (IsString (fromString))
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Network.HTTP.Simple
import Penny.Types
import qualified System.Clock as C
import System.Fuse
import Text.Feed.Import (parseFeedSource)
import Text.Feed.Query
import Text.Feed.Types

-- | An IO action whose value is stored to minimize repeat calls.
data Cached a = Cached (IO a) C.TimeSpec (IORef (C.TimeSpec, a))

-- | Cache an IO action so that repeated calls use the same value
cache ::
  IO a ->
  -- | The action to cache
  C.TimeSpec ->
  -- | The minimum time between calls
  IO (Cached a)
cache act diff = do
  val <- act
  t <- C.getTime C.Monotonic
  ref <- newIORef (t, val)
  return $ Cached act diff ref

-- | Get a cached value
access :: Cached a -> IO a
access (Cached act diff ref) = do
  (t, val) <- readIORef ref
  now <- C.getTime C.Monotonic
  if ((now `C.diffTimeSpec` t) > diff)
    then do
      newVal <- act
      writeIORef ref (now, newVal)
      return newVal
    else return val

newtype ItemFile = ItemFile Item

instance Fileable ItemFile where
  toFile (ItemFile i) = return . encodeUtf8 . fromMaybe "" . getItemDescription $ i
  getName (ItemFile i) = return . T.unpack . fromMaybe "" . getItemTitle $ i
  getTime _ = return $ epoch -- FIXME

newtype FeedDir = FeedDir (Cached Feed)

reed :: (MonadIO m) => String -> m Feed
reed url = fromJust . parseFeedSource . fromStrict . getResponseBody <$> httpBS (fromString url)

-- | Access an Atom or RSS feed
feed ::
  -- | The time to wait before updating the feed
  C.TimeSpec ->
  -- | The URL of the feed
  String ->
  IO DirectType
feed diff x = DirectType . FeedDir <$> (flip cache diff . reed $ x)

instance Fileable FeedDir where
  getName (FeedDir f) = T.unpack . getFeedTitle <$> access f
  getTime (FeedDir f) = return epoch -- FIXME: This should be the feed date,
  -- but their in a weird text format
  -- that I'll need to parse.

instance Directable FeedDir where
  getFiles (FeedDir f) = return . map (FileType . ItemFile) . feedItems =<< access f
  getDirPath _ "" = throwE eNOENT
  getDirPath repo "/" = return . Left . DirectType $ repo
  getDirPath (FeedDir f) (_ : path) = do
    except . headEither eNOENT . map (Right . FileType . ItemFile) . filter ((== Just (T.pack path)) . getItemTitle) . feedItems =<< liftIO (access f)
