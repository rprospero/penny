{-# LANGUAGE ExistentialQuantification #-}

-- | Types and helpers for writing penny modules
module Penny.Types
  ( Fileable (..),
    FileType (FileType),
    epoch,
    ghOps,
    subpath,
    headEither,
    Directable (..),
    DirectType (DirectType),
    FolderCollection (..),
  )
where

import Control.Monad (filterM)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Trans.Except
import qualified Data.ByteString as BS hiding
  ( pack,
  )
import Data.List (isPrefixOf)
import Data.Time.Clock
import Data.Time.Clock.System
import System.FilePath
import System.Fuse
import System.Posix.Files
import System.Posix.Types

-- | This typeclass represents data which can be converted into a
-- single file.
class Fileable a where
  toFile :: a -> IO BS.ByteString
  toFile _ = return ""
  getTime :: a -> IO UTCTime
  getTime _ = return epoch
  getName :: a -> IO FilePath

-- | An Existential wrapper around the Fileable typeclass
data FileType = forall a. (Fileable a) => FileType a

-- | Class for data types that can be treated as directories on the filesystem.
class Fileable a => Directable a where
  getFiles :: a -> IO [FileType]
  getFiles _ = return []
  getSubs :: a -> IO [DirectType]
  getSubs _ = return []
  getDirPath :: a -> FilePath -> ExceptT Errno IO (Either DirectType FileType)

-- | An Existential wrapper for Directable.
data DirectType = forall a. (Directable a) => DirectType a

ghRead ::
  String ->
  FileType ->
  ByteCount ->
  FileOffset ->
  IO (Either Errno BS.ByteString)
ghRead _ (FileType f) byteCount offset = Right . BS.take (fromIntegral byteCount) . BS.drop (fromIntegral offset) <$> toFile f

ghGetFileSystemStats :: (Monad m) => p -> m (Either a FileSystemStats)
ghGetFileSystemStats _ =
  return $
    Right $
      FileSystemStats
        { fsStatBlockSize = 512,
          fsStatBlockCount = 1,
          fsStatBlocksFree = 1,
          fsStatBlocksAvailable = 1,
          fsStatFileCount = 5,
          fsStatFilesFree = 10,
          fsStatMaxNameLength = 255
        }

ghOpen ::
  (Directable a) =>
  a ->
  FilePath ->
  OpenMode ->
  flags ->
  IO (Either Errno FileType)
ghOpen token path mode _ = runExceptT $ do
  case mode of
    ReadOnly -> do
      f <- getDirPath token path
      case f of
        Left _ -> throwE eNOENT
        Right x -> return x
    _ -> throwE eNOENT

-- | The first moment of the UNIX epoch as a UTCTime
epoch :: UTCTime
epoch = UTCTime systemEpochDay 0

dirStat :: (Directable a, MonadIO m) => a -> FuseContext -> m FileStat
dirStat dir ctx = do
  t <- liftIO $ getTime dir
  return $
    FileStat
      { statEntryType = Directory,
        statFileMode =
          foldr1
            unionFileModes
            [ ownerReadMode,
              ownerExecuteMode,
              groupReadMode,
              groupExecuteMode,
              otherReadMode,
              otherExecuteMode
            ],
        statLinkCount = 2,
        statFileOwner = fuseCtxUserID ctx,
        statFileGroup = fuseCtxGroupID ctx,
        statSpecialDeviceID = 0,
        statFileSize = 4096,
        statBlocks = 1,
        statAccessTime = 0,
        statModificationTime =
          fromInteger
            . floor
            . diffUTCTime t
            $ epoch,
        statStatusChangeTime = 0
      }

fileStat :: (Fileable a, MonadIO m) => a -> FuseContext -> m FileStat
fileStat src ctx = do
  t <- liftIO $ getTime src
  f <- liftIO $ toFile src
  return $
    FileStat
      { statEntryType = RegularFile,
        statFileMode =
          foldr1
            unionFileModes
            [ownerReadMode, groupReadMode, otherReadMode],
        statLinkCount = 1,
        statFileOwner = fuseCtxUserID ctx,
        statFileGroup = fuseCtxGroupID ctx,
        statSpecialDeviceID = 0,
        statFileSize = fromIntegral . BS.length $ f,
        statBlocks = 1,
        statAccessTime = 0,
        statModificationTime =
          fromInteger
            . floor
            . diffUTCTime t
            $ epoch,
        statStatusChangeTime = 0
      }

ghGetFileStat ::
  (Directable a) =>
  a ->
  FilePath ->
  ExceptT Errno IO FileStat
ghGetFileStat dir "/" = do
  ctx <- liftIO $ getFuseContext
  dirStat dir ctx
ghGetFileStat dir path = do
  ctx <- liftIO getFuseContext
  file <- getDirPath dir path
  case file of
    Right (FileType f) -> fileStat f $ ctx
    Left (DirectType d) -> dirStat d $ ctx

ghOpenDirectory :: FilePath -> IO Errno
ghOpenDirectory "/" = return eOK
ghOpenDirectory _ = return eOK

ghReadDirectory ::
  (Directable a) =>
  a ->
  FilePath ->
  IO (Either Errno [(FilePath, FileStat)])
ghReadDirectory dir "/" = do
  ctx <- getFuseContext
  files <- mapM (statInfo ctx) =<< getFiles dir
  subs <- mapM (statDirInfo ctx) =<< getSubs dir
  dstat <- dirStat dir ctx
  return . Right $ [(".", dstat), ("..", dstat)] <> files <> subs
ghReadDirectory dir path = runExceptT $ do
  subs <- liftIO . getSubs $ dir
  name <- except . headEither eNOENT . drop 1 . splitDirectories $ path
  dirs <- filterM (\(DirectType x) -> (== name) <$> liftIO (getName x)) $ subs
  (DirectType sub) <- except . headEither eNOENT $ dirs

  ExceptT . liftIO . (ghReadDirectory sub) . subpath $ path

-- | A set of FuseOperations that can be run in fuseMain, based on the
-- given directory.
ghOps :: Directable a => a -> FuseOperations FileType
ghOps dir =
  defaultFuseOps
    { fuseGetFileStat = runExceptT . ghGetFileStat dir,
      fuseOpen = ghOpen dir,
      fuseRead = ghRead,
      fuseOpenDirectory = ghOpenDirectory,
      fuseReadDirectory = ghReadDirectory dir,
      fuseGetFileSystemStats = ghGetFileSystemStats
    }

-- | Remove the top most folder from a file path.
-- |
-- | subpath "/foo/bar/baz" == "/bar/baz"
subpath :: FilePath -> FilePath
subpath = joinPath . dropSecond . splitDirectories

dropSecond :: [a] -> [a]
dropSecond [] = []
dropSecond [a] = [a]
dropSecond (a : _ : xs) = a : xs

-- | Take the head of the list with a given error value for an empty list
headEither ::
  -- | Error value
  e ->
  -- | List
  [a] ->
  Either e a
headEither err [] = Left err
headEither _ (x : _) = Right x

statInfo :: MonadIO m => FuseContext -> FileType -> m (FilePath, FileStat)
statInfo ctx (FileType f) = (,) <$> liftIO (getName f) <*> fileStat f ctx

statDirInfo :: (MonadIO m) => FuseContext -> DirectType -> m (FilePath, FileStat)
statDirInfo ctx (DirectType f) = (,) <$> liftIO (getName f) <*> dirStat f ctx

-- | A collection of different virtual directories.
data FolderCollection = FC [DirectType]

instance Fileable FolderCollection where
  getName _ = return "repos"

instance Directable FolderCollection where
  getSubs (FC folders) = return folders
  getDirPath _ "" = throwE eNOENT
  getDirPath col "/" = return . Left . DirectType $ col
  getDirPath (FC folders) path = do
    options <- filterM match $ folders
    DirectType r <- except . headEither eNOENT $ options
    getDirPath r . subpath $ path
    where
      match :: (MonadIO m) => DirectType -> m Bool
      match (DirectType r) = do
        name <- liftIO $ getName r
        return $ ("/" <> name) `isPrefixOf` path
