{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.ByteString as BS hiding
  ( pack,
  )
import qualified Data.ByteString.Char8 as BS
import Penny
import System.Clock
import System.Fuse
import System.Process

-- | Run a system command and get the program's output
process :: String -> IO BS.ByteString
process cmd = do
  (_, key, _) <- readCreateProcessWithExitCode (shell cmd) ""
  return . BS.pack . head . lines $ key

folders :: [String]
folders = ["disorderedmaterials", "rprospero", "mantidproject", "sasview"]

feeds :: [String]
feeds = ["https://feedforall.com/sample.xml", "https://www.theguardian.com/uk/rss"]

main :: IO ()
main = do
  token <- process "pass github_message_token"
  fs <- mapM (feed (fromNanoSecs $ 5 * 60 * 1000000000)) feeds
  fuseMain (ghOps (FC (fs <> (map (DirectType . UserRepos token) $ folders)))) defaultExceptionHandler
