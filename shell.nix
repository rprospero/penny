{ pkgs ? import <nixpkgs> { } }:

let

in pkgs.mkShell {
  buildInputs = [
    pkgs.cabal-install
    (pkgs.haskellPackages.ghcWithHoogle (pkgs: [
      pkgs.Cabal
      pkgs.brittany
      pkgs.clock
      pkgs.feed
      pkgs.github
      pkgs.hlint
      pkgs.haskell-language-server
      pkgs.HFuse
      pkgs.http-conduit
    ]))
  ];
}
